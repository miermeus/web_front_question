use crate::*;

#[derive(Properties, Clone, PartialEq, serde::Deserialize)]
pub struct QuestionStruct {
    pub id: String,
    pub title: String,
    pub content: String,
    pub tags: Option<HashSet<String>>,
}

impl QuestionStruct {
    pub async fn get_question(key: Option<String>) -> Msg {
        let request = match &key {
            None => "http://localhost:3000/questions".to_string(),
            Some(ref key) => format!("http://localhost:3000/questions/{}", key),
        };
        let response = http::Request::get(&request).send().await;
        match response {
            Err(e) => Msg::GotQuestion(Err(e)),
            Ok(data) => Msg::GotQuestion(data.json().await),
        }
    }
}

pub fn format_tags(tags: &HashSet<String>) -> String {
    let taglist: Vec<&str> = tags.iter().map(String::as_ref).collect();
    taglist.join(", ")
}

#[derive(Properties, Clone, PartialEq, serde::Deserialize)]
pub struct QuestionProps {
    pub question: QuestionStruct,
}

#[function_component(Question)]
pub fn question(question: &QuestionProps) -> Html {
    let question = &question.question;
    html! {
        <>
            <div class="question">
                <h3>{ &question.title }</h3>
                <p>{ &question.content }</p>
            </div>
            <div class="annotation">
                {format!("[id: {}", &question.id)}
                {if let Some(ref tags) = question.tags {
                    format!("; tags: {}", format_tags(tags))
                } else {
                    "".to_string()
                }}
                {"]"}
            </div>
        </>
    }
}
